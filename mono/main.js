const express = require('express')
const app = express()

const PORT = process.env.PORT || 3000

const bodyParser = require('body-parser');

app.use(bodyParser.json());

const region = "eu-west-1";

const AWS = require("aws-sdk")
AWS.config.update({
  region: region
});
const docClient = new AWS.DynamoDB.DocumentClient()

const _ = require("lodash");

function badRequest(msg, res) {
  res.status(400).send(msg);
}

function ok(msg, res) {
  res.status(200).send(msg);
}

function error(msg, res) {
  res.status(500).send(msg);
}

function collect(driverId, res) {
  console.log("collect called for driverId " + driverId)
  docClient.query({
    TableName: "point_cache",
    IndexName: "driverId-index",
    KeyConditionExpression: "driverId = :driverid",
    ExpressionAttributeValues: {
      ":driverid": driverId
    }
  }, (err, data) => {
    if(err) {
      error(JSON.stringify(err), res);
    } else {

      let groups = _.toPairs(_.groupBy(data.Items, (i) => i.driverId));

      let filtered = _.filter(groups, g => {
        let points = _.sortBy(g[1], e => e.ts)
        console.log(((Date.now() / 1000) - 60) + " >= " + points[points.length-1].ts)
        return ((Date.now() / 1000) - 60) >= points[points.length-1].ts;
      })

      let pointsToDelete = [];

      let trips = _.map(filtered, t => {
        let points = _.sortBy(t[1], e => e.ts);
        let start = points[0].ts
        let end = points[points.length-1].ts
        let driverId = points[0].driverId

        pointsToDelete = pointsToDelete.concat(points);

        return {
          tripid: Number.parseInt("" + start + end + t[0]),
          start: start,
          end: end,
          points: JSON.stringify(_.map(points, p => { 
            return {
              ts: p.ts, 
              lat: p.lat, 
              lon: p.lon
            }
          })),
          driverid: driverId
        }
      });

      _.forEach(trips, t => {
        docClient.put({
          TableName: "trips",
          Item: t
        }, (err, data) => {
          if(err) {
            console.log("Error inserting trip: ", err);
          } else {
            console.log("inserted trip with id " + t.tripid);
          }
        })
      })

      _.forEach(pointsToDelete, p => {
        console.log("deleting point " + p.id)
        docClient.delete({
          TableName: "point_cache",
          Key: {
            id: p.id
          }
        }, function(err, data) {
          if (err) {
            console.error("Error Point: ", JSON.stringify(p, null, 2))
            console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
          }
        })
      })

      console.log("Deleted " + pointsToDelete.length + " of " + data.Items.length + " records!");
      ok(JSON.stringify(trips), res);
    }
  })
}

function write(point, res) {
  console.log("inserting: " + JSON.stringify(point))
  
  docClient.put({
      "TableName": "point_cache",
      "Item": {
          "id": "" + Date.now() + point.driverId,
          "driverId": point.driverId,
          "ts": point.ts,
          "lat": point.lat,
          "lon": point.lon,
          "ttl": point.ts + 3600
      }
  }, (err, data) => {
      if(err) {
          error(JSON.stringify(err), res)
      } else {
          ok("Added: " + JSON.stringify(data, null, 2), res)
      }
  })
}

function haversine() {
  var radians = Array.prototype.map.call(arguments, function(deg) { return deg/180.0 * Math.PI; });
  var lat1 = radians[0], lon1 = radians[1], lat2 = radians[2], lon2 = radians[3];
  var R = 6372.8; // km
  var dLat = lat2 - lat1;
  var dLon = lon2 - lon1;
  var a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLon / 2) * Math.sin(dLon /2) * Math.cos(lat1) * Math.cos(lat2);
  var c = 2 * Math.asin(Math.sqrt(a));
  return R * c;
}

function distance(points) {
  let distance = 0;
  for(let n = 0; n < 100; n++) {
    for(i = 0; i < points.length-1; i++) {
      let p1 = points[i];
      let p2 = points[i+1];
      distance+=haversine(p1.lat, p1.lon, p2.lat, p2.lon);
    }
  }
  return distance / 1000;
}

function query(driverId, wantsPoints, count, res) {
  console.log("query called with arguments: " + 
    JSON.stringify({'driverId': driverId, 'wantsPoints': wantsPoints, 'count': count}))
  docClient.query({
    TableName: "trips",
    KeyConditionExpression: "driverid = :driverid",
    ExpressionAttributeValues: {
      ":driverid": driverId
    },
    Limit: count
  }, (err, data) => {
    if(err) {
      error(JSON.stringify(err), res);
    } else {
      ok(JSON.stringify(_.map(data.Items, trip => {
        let points = JSON.parse(trip.points);
        if(wantsPoints) {
          trip.points = points;
        } else {
          trip.points = undefined;
        }
        trip.distance = distance(points);
        trip.duration = trip.end - trip.start;
        return trip;
      })), res);
    }
  })
}

app.post('/collect-single', (req, res) => {
  let driverId = parseInt(req.query["driverId"]);
  collect(driverId, res);
});

function parseIntOrDefault(str, def) {
  let i = parseInt(str);
  if(str === null || str === undefined || str === NaN) {
    return def;
  } else {
    return i;
  }
}

app.get('/query', (req, res) => {
  let driverId = parseInt(req.query["driverId"]);
  let count = parseIntOrDefault(req.query["count"], 10);
  let wantsPoints = req.query["points"] !== undefined;
  query(driverId, wantsPoints, count, res);
})

app.post('/write', (req, res) => {
  console.log(req.body);
  if(req.body === undefined) {
    badRequest("no body", res)
    return
  }

  let body = req.body;
  if(body.point === undefined) {
      badRequest("point invalid", res)
  } else if(body.point.ts === undefined || (!body.point.ts instanceof Number)) {
      badRequest("ts invalid", res)
  } else if(body.point.driverId === undefined || (!body.point.driverId instanceof Number)) {
      badRequest("driverId invalid", res)
  } else if(body.point.lat === undefined || (!body.point.lat instanceof Number)) {
      badRequest("lat invalid", res)
  } else if(body.point.lon === undefined || (!body.point.lon instanceof Number)) {
      badRequest("lon invalid", res)
  } else {
    write(body.point, res);
  }
})
app.listen(3000);