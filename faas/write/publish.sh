zip -r write.zip write.js node_modules/
aws lambda update-function-code --function-name write --zip-file fileb://$PWD/write.zip
rm write.zip
