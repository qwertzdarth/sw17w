const AWS = require("aws-sdk")
const docClient = new AWS.DynamoDB.DocumentClient()

function respond(code, msg, callback) {
    callback(null, {
        "statusCode": code,
        "body": msg,
        "isBase64Encoded": false
    });
}

function ok(msg, callback) {
    respond(200, msg, callback)
}

function badRequest(msg, callback) {
    respond(400, msg, callback)
}

exports.handler = function(event, context, callback) {
    if(event.body === undefined) {
        badRequest("no body", callback)
        return
    }

    let body = JSON.parse(event.body);
    if(body.point === undefined) {
        badRequest("point invalid", callback)
    } else if(body.point.ts === undefined || (!body.point.ts instanceof Number)) {
        badRequest("ts invalid", callback)
    } else if(body.point.driverId === undefined || (!body.point.driverId instanceof Number)) {
        badRequest("driverId invalid", callback)
    } else if(body.point.lat === undefined || (!body.point.lat instanceof Number)) {
        badRequest("lat invalid", callback)
    } else if(body.point.lon === undefined || (!body.point.lon instanceof Number)) {
        badRequest("lon invalid", callback)
    } else {
        console.log("inserting: " + JSON.stringify(body.point))

        docClient.put({
            "TableName": "point_cache",
            "Item": {
                "id": "" + Date.now() + body.point.driverId,
                "driverId": body.point.driverId,
                "ts": body.point.ts,
                "lat": body.point.lat,
                "lon": body.point.lon,
                "ttl": body.point.ts + 3600
            }
        }, (err, data) => {
            if(err) {
                respond(500, JSON.stringify(err), callback)
            } else {
                ok("Added: " + JSON.stringify(data, null, 2), callback)
            }
        })
    }
}
