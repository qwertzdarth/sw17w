zip -r collect.zip collect.js node_modules/
aws lambda update-function-code --function-name collect --zip-file fileb://$PWD/collect.zip
rm collect.zip
