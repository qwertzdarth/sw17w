const AWS = require("aws-sdk")
const docClient = new AWS.DynamoDB.DocumentClient()
const _ = require("lodash")

function respond(code, msg, callback) {
    callback(null, {
        "statusCode": code,
        "body": msg,
        "isBase64Encoded": false
    });
}

function ok(msg, callback) {
    respond(200, msg, callback)
}

function badRequest(msg, callback) {
    respond(400, msg, callback)
}

function haversine() {
  var radians = Array.prototype.map.call(arguments, function(deg) { return deg/180.0 * Math.PI; });
  var lat1 = radians[0], lon1 = radians[1], lat2 = radians[2], lon2 = radians[3];
  var R = 6372.8; // km
  var dLat = lat2 - lat1;
  var dLon = lon2 - lon1;
  var a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLon / 2) * Math.sin(dLon /2) * Math.cos(lat1) * Math.cos(lat2);
  var c = 2 * Math.asin(Math.sqrt(a));
  return R * c;
}

function distance(points) {
  let distance = 0;
  for(i = 0; i < points.length-1; i++) {
    let p1 = points[i];
    let p2 = points[i+1];
    distance+=haversine(p1.lat, p1.lon, p2.lat, p2.lon);
  }
  return distance;
}

exports.handler = function(event, context, callback) {
  let driverId = parseInt(event.queryStringParameters.driverId)
  let wantsPoints = event.queryStringParameters.points
  console.log("query called with arguments: " + {'driverId': driverId, 'wantsPoints': wantsPoints})
  docClient.query({
    TableName: "trips",
    KeyConditionExpression: "driverid = :driverid",
    ExpressionAttributeValues: {
      ":driverid": driverId
    }
  }, (err, data) => {
    if(err) {
      respond(500, JSON.stringify(err), callback);
    } else {
      respond(200, JSON.stringify(_.map(data.Items, trip => {
        let points = JSON.parse(trip.points);
        if(wantsPoints) {
          trip.points = points;
        } else {
          trip.points = undefined;
        }
        trip.distance = distance(points);
        trip.duration = trip.end - trip.start;
        return trip;
      })), callback);
    }
  })
}