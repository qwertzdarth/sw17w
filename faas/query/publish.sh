zip -r query.zip query.js node_modules/
aws lambda update-function-code --function-name query --zip-file fileb://$PWD/query.zip
rm query.zip
