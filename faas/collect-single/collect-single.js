const AWS = require("aws-sdk")
const docClient = new AWS.DynamoDB.DocumentClient()
const _ = require("lodash")

function respond(code, msg, callback) {
    callback(null, {
        "statusCode": code,
        "body": msg,
        "isBase64Encoded": false
    });
}

function ok(msg, callback) {
    respond(200, msg, callback)
}

function badRequest(msg, callback) {
    respond(400, msg, callback)
}

exports.handler = function(event, context, callback) {
  let driverId = parseInt(event.queryStringParameters.driverId)
  console.log("collect called for driverId " + driverId)
  docClient.query({
    TableName: "point_cache",
    IndexName: "driverId-index",
    KeyConditionExpression: "driverId = :driverid",
    ExpressionAttributeValues: {
      ":driverid": driverId
    }
  }, (err, data) => {
    if(err) {
      respond(500, JSON.stringify(err), callback);
    } else {

      let groups = _.toPairs(_.groupBy(data.Items, (i) => i.driverId));

      let filtered = _.filter(groups, g => {
        let points = _.sortBy(g[1], e => e.ts)
        console.log(((Date.now() / 1000) - 60) + " >= " + points[points.length-1].ts)
        return ((Date.now() / 1000) - 60) >= points[points.length-1].ts;
      })

      let pointsToDelete = [];

      let trips = _.map(filtered, t => {
        let points = _.sortBy(t[1], e => e.ts);
        let start = points[0].ts
        let end = points[points.length-1].ts
        let driverId = points[0].driverId

        pointsToDelete = pointsToDelete.concat(points);

        return {
          tripid: Number.parseInt("" + start + end + t[0]),
          start: start,
          end: end,
          points: JSON.stringify(_.map(points, p => { 
            return {
              ts: p.ts, 
              lat: p.lat, 
              lon: p.lon
            }
          })),
          driverid: driverId
        }
      });

      _.forEach(trips, t => {
        docClient.put({
          TableName: "trips",
          Item: t
        }, (err, data) => {
          if(err) {
            console.log("Error inserting trip: ", err);
          } else {
            console.log("inserted trip with id " + t.tripid);
          }
        })
      })

      _.forEach(pointsToDelete, p => {
        console.log("deleting point " + p.id)
        docClient.delete({
          TableName: "point_cache",
          Key: {
            id: p.id
          }
        }, function(err, data) {
          if (err) {
            console.error("Error Point: ", JSON.stringify(p, null, 2))
            console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
          }
        })
      })

      console.log("Deleted " + pointsToDelete.length + " of " + data.Items.length + " records!");
      respond(200, JSON.stringify(trips), callback);
    }
  })
}