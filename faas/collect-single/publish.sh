zip -r collect-single.zip collect-single.js node_modules/
aws lambda update-function-code --function-name collect-single --zip-file fileb://$PWD/collect-single.zip
rm collect-single.zip
