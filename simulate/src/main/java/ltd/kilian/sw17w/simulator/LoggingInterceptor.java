package ltd.kilian.sw17w.simulator;

import okhttp3.Interceptor;
import okhttp3.Response;

import java.io.IOException;
import java.io.PrintStream;
import java.time.Instant;

public class LoggingInterceptor implements Interceptor {
    private final PrintStream out;

    public LoggingInterceptor(PrintStream out) {
        this.out = out;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        long start = System.currentTimeMillis();
        Response res = chain.proceed(chain.request());
        long end = System.currentTimeMillis();

        long duration = end - start;

        out.println(Instant.now().getEpochSecond() + "," + chain.request().url().toString() + "," + duration);

        return res;
    }
}
