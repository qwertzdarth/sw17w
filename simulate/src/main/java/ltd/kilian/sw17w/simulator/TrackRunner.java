package ltd.kilian.sw17w.simulator;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class TrackRunner implements Runnable {
    private final List<Track> tracks;
    private boolean running = true;
    private final Random random = new Random();
    private final long driverId = random.nextLong();
    private final BiConsumer<Integer, Point> gotPoint;
    private final Consumer<Integer> finishedTrip;
    private final int sleepTime;
    private final int id;
    private final AtomicInteger pointsProcessed;
    private final static int ACCELERATION = 10;
    private final static int TIME_BETWEEN_TRIPS = 15;

    public TrackRunner(int id, List<Track> tracks, int sleepTime, BiConsumer<Integer, Point> gotPoint, Consumer<Integer> finishedTrip, AtomicInteger pointsProcessed) {
        this.tracks = tracks;
        this.sleepTime = sleepTime;
        this.gotPoint = gotPoint;
        this.finishedTrip = finishedTrip;
        this.id = id;
        this.pointsProcessed = pointsProcessed;
    }

    private Track choose() {
        return tracks.get(random.nextInt(tracks.size()));
    }

    private void log(String msg) {
        System.out.println(String.format("TrackRunner (%d): %s", id, msg));
    }

    @Override
    public void run() {
        Track current = choose();
        int currentIdx = 0;
        log("Starting trip (duration: " + (Duration.between(current.start(), current.end()).toString()) + ", distance: " + current.distance() + ")");
        while (running) {
            Point p = current.points().get(currentIdx);
            //log("sending point");
            gotPoint.accept(id, p);
            currentIdx++;
            pointsProcessed.incrementAndGet();

            if (currentIdx >= current.points().size()) {
                finishedTrip.accept(id);
                log("Finished Trip " + currentIdx);
                try {
                    Thread.sleep(TIME_BETWEEN_TRIPS * 1000);
                } catch (InterruptedException e) {
                    break;
                }
                current = choose();
                currentIdx = 0;
                log("Starting trip (duration: " + (Duration.between(current.start(), current.end()).toString()) + ", distance: " + current.distance() + ")");
            } else {
                try {
                    Thread.sleep(Math.min(60000, Duration.between(p.ts(), current.points().get(currentIdx).ts()).toMillis() / ACCELERATION));
                } catch (InterruptedException e) {
                    log("Interrupted!");
                    break;
                }
            }
        }
    }

    public void stop() {
        running = false;
    }
}
