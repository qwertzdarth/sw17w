package ltd.kilian.sw17w.simulator;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.*;

public class Track {
    private final static double R = 6371e3;

    private final int id;
    private final LocalDateTime start, end;
    private final List<Point> points;
    private final double distance;

    private Track(int id, LocalDateTime start, LocalDateTime end, List<Point> points, double distance) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.points = points;
        this.distance = distance;
    }

    public int id() {
        return id;
    }

    public LocalDateTime start() {
        return start;
    }

    public LocalDateTime end() {
        return end;
    }

    public List<Point> points() {
        return points;
    }

    public double distance() {
        return distance;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private LocalDateTime start, end;
        private List<Point> points;

        private Builder() {
            points = new ArrayList<>();
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }


        public Builder addPoint(Point p) {
            if(points.isEmpty()) {
                start = p.ts();
            }
            points.add(p);
            end = p.ts();
            return this;
        }

        public boolean hasPoints() {
            return !points.isEmpty();
        }

        public LocalDateTime end() {
            return end;
        }

        public Track build() {
            if(id == null) throw new IllegalStateException("id may not be null!");
            if(start == null) throw new IllegalStateException("start may not be null!");
            if(end == null) throw new IllegalStateException("end may not be null!");
            if(points.isEmpty()) throw new IllegalStateException("points may not be empty!");

            return new Track(id, start, end, Collections.unmodifiableList(points), distance(points));
        }

    }

    @Override
    public String toString() {
        return "Track{" +
                "id=" + id +
                ", start=" + start +
                ", end=" + end +
                ", points=" + points.size() +
                ", distance=" + String.format("%.2fm", distance/1e0) +
                ", duration=" + duration().toMinutes() +
                '}';
    }

    private Duration duration() {
        return Duration.between(start, end);
    }

    private static double distance(List<Point> ps) {
        double distance = 0;
        for(int i = 0; i < ps.size() - 1; i++) {
            distance += haversine(ps.get(i), ps.get(i+1));
        }
        return distance;
    }

    public static double haversine(Point p1, Point p2) {
        double dLat = Math.toRadians(p2.lat() - p1.lat());
        double dLon = Math.toRadians(p2.lon() - p1.lon());
        double lat1 = Math.toRadians(p1.lat());
        double lat2 = Math.toRadians(p2.lat());

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }
}
