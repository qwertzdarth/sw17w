package ltd.kilian.sw17w.simulator;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    private static final Duration TIME_BETWEEN_POINTS = Duration.of(1, ChronoUnit.MINUTES);
    private static final Duration MINIMUM_TRIP_DURATION = Duration.of(10, ChronoUnit.MINUTES);
    private static final int MINIMUM_TRIP_SIZE = 200;
    private static final double MINIMUM_TRIP_DISTANCE = 1 * 1e3;

    private static final AtomicInteger tripId = new AtomicInteger(0);
    private static final AtomicInteger pointsProcessed = new AtomicInteger(0);

    private static Stream<String[]> points(Path location) throws IOException {
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(location.toFile())).build()) {
            return reader.readAll().stream();
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 4) System.exit(1);

        // "https://0rgm0n5hz8.execute-api.eu-west-1.amazonaws.com/prod"
        String baseUrl = args[0];
        int start = Integer.parseInt(args[2]);
        int end = Integer.parseInt(args[3]);

        Map<Integer, List<Point>> points = points(Paths.get(args[1]))
                //.limit(3)
                .skip(1)
                .map(Point::parse)
                .flatMap(p -> p.map(Stream::of).orElse(Stream.empty()))
                .collect(Collectors.groupingBy(Point::trackId));

        List<Track> tracks = points.entrySet().stream()
                .map(e -> track(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        System.out.println("There are " + tracks.size() + " tracks!");

        try (PrintStream ps = new PrintStream(new File("output.log"))) {
            final OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new LoggingInterceptor(ps))
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .writeTimeout(5, TimeUnit.SECONDS)
                    .readTimeout(5, TimeUnit.SECONDS)
                    .build();

            System.out.println("Starting " + (end - start) + " track runners!");
            final ExecutorService executor = Executors.newWorkStealingPool(end - start);

            AtomicBoolean readerRunning = new AtomicBoolean(true);

            Thread readerThread = new Thread(() -> {
                Random readerRandom = new Random(10);
                while (readerRunning.get()) {
                    try {
                        int driverId = start + readerRandom.nextInt(end-start);

                        Request req = new Request.Builder()
                                .url(baseUrl + "/query?driverId=" + driverId + "&count=100&points")
                                .get()
                                .build();

                        Response res = client.newCall(req).execute();
                        String resBodyStr = res.body().string();
                        // System.out.println(res.toString());
                        Thread.sleep(100);
                    } catch (Exception e) {
                        System.err.println("Query: " + e.getMessage());
                    }
                }
            });

            readerThread.start();

            IntStream.range(start, end).forEach(i -> {
                System.out.println("Starting track runner " + i);
                TrackRunner tr = new TrackRunner(i, tracks, 100, onPoint(client, baseUrl), onFinish(client, baseUrl), pointsProcessed);
                executor.submit(tr);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {

                }
            });

            int seconds = 0;
            while (true) {
                Thread.sleep(1000);
                seconds += 1;
                System.out.println(String.format("Points/s: %.1f", ((double) pointsProcessed.get() / (double) seconds)));
                pointsProcessed.set(0);
                seconds = 0;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
     }

    private static BiConsumer<Integer, Point> onPoint(OkHttpClient client, String baseUrl) {
        return (id, point) -> {
            String body = "{\"point\": {\"ts\": " + Instant.now().atZone(ZoneId.of("UTC")).toEpochSecond() + ", \"driverId\":" +
                    id + ", \"lat\": " + point.lat() + ", \"lon\": " + point.lon() + " }}";

            // System.out.println(body);

            Request req = new Request.Builder()
                    .url(baseUrl + "/write")
                    .post(RequestBody.create(MediaType.parse("application/json"), body))
                    .build();

            try {
                Response res = client.newCall(req).execute();
                if (res.code() != 200) {
                    System.err.println("Invalid response " + res.code() + " \"" + res.body().string() + "\" for point: " + point);
                }
                res.close();
            } catch (IOException e) {
                System.err.println("Write: " + e);
            }
        };
    }

    private static Consumer<Integer> onFinish(OkHttpClient client, String baseUrl) {
        return (id) -> {
            Request req = new Request.Builder()
                    .url(baseUrl + "/collect-single?driverId=" + id)
                    .post(RequestBody.create(MediaType.parse("text/plain"), ""))
                    .build();
            try {
                Response res = client.newCall(req).execute();
                if (res.code() != 200) {
                    System.err.println("Invalid response " + res.code() + " \"" + res.body().string() + "\" for driverId: " + id);
                }
                res.close();
            } catch (IOException e) {
                System.err.println("Finish: " + e);
            }
        };
    }

    private static Track track(int id, List<Point> ps) {
        Track.Builder track = Track.builder()
                .id(id);

        ps.sort(Comparator.comparing(Point::ts));

        ps.forEach(track::addPoint);
        return track.build();
    }

    private static Stream<Track> trips(List<Point> points) {
        if (points.isEmpty()) return Stream.empty();

        points.sort(Comparator.comparing(Point::ts));

        List<Track> result = new ArrayList<>();
        Track.Builder trip = Track.builder().id(tripId.getAndIncrement());
        for (Point p : points) {
            // trip is finished
            if (trip.hasPoints() && trip.end().plus(TIME_BETWEEN_POINTS).isBefore(p.ts())) {
                result.add(trip.build());
                trip = Track.builder()
                        .id(tripId.getAndIncrement())
                        .addPoint(p);
            } else { // trip is not finished
                trip.addPoint(p);
            }
        }

        return result.stream()
                // .filter(t -> t.points().size() >= MINIMUM_TRIP_SIZE)
                // .filter(t -> t.start().plus(MINIMUM_TRIP_DURATION).isBefore(t.end()))
                .filter(t -> t.distance() >= MINIMUM_TRIP_DISTANCE);
    }
}
