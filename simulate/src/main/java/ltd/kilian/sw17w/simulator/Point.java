package ltd.kilian.sw17w.simulator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class Point {
    private final int trackId;
    private final LocalDateTime ts;
    private final double lat;
    private final double lon;

    private Point(int trackId, LocalDateTime ts, double lat, double lon) {
        this.trackId = trackId;
        this.ts = ts;
        this.lat = lat;
        this.lon = lon;
    }

    public int trackId() {
        return trackId;
    }

    public LocalDateTime ts() {
        return ts;
    }

    public double lat() {
        return lat;
    }

    public double lon() {
        return lon;
    }

    public static Optional<Point> parse(String[] line) {
        try {
            int trackId = Integer.parseInt(line[3]);
            LocalDateTime ldt = LocalDateTime.parse(line[4], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            double lat = Double.parseDouble(line[1]);
            double lon = Double.parseDouble(line[2]);

            return Optional.of(new Point(trackId, ldt, lat, lon));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public String toString() {
        return "Point{" +
                "id=" + trackId +
                ", ts=" + ts +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
